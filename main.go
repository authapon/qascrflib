package qascrflib

import (
	"fmt"
	moosql "gitlab.com/authapon/moosqlite"
	"math"
	"regexp"
	"strings"
	"unicode/utf8"
)

type (
	WordSegMatch struct {
		MatchWord string
		RestData  string
	}

	WordSegResult struct {
		Unknown int
		Data    string
	}

	WordSegResult2 struct {
		Unknown  int
		Data     string
		RestData string
	}

	CrfTrainSentence struct {
		Sentence []string
		Label    []string
	}

	CrfFeature struct {
		Feature2  string
		Feature1  string
		Feature0  string
		Feature1b string
		Feature2b string
		Word      string
		W         float64
	}

	ChunkCrfSentence struct {
		Sentence []string
		Label    []string
		Chunk    []string
	}

	ChunkCrfFeature struct {
		L1   string
		L0   string
		POS1 string
		POS0 string
		W1   string
		W0   string
		W    float64
	}

	Words map[string][]string
)

var (
	WordLeft       = 3
	WordRight      = 3
	ChunkLeft      = 3
	ChunkRight     = 3
	tokenSize      = 3
	tokenChunkSize = 5
)

func GenWordForTrain(sentence []string, k int) []string {
	NewSentence := make([]string, 0)
	lastIndex := k + 2
	firstIndex := k - 2
	if len(sentence)-1 < lastIndex {
		lastIndex = len(sentence) - 1
	}
	if k < 2 {
		firstIndex = 0
	}
	for i := firstIndex; i <= lastIndex; i++ {
		NewSentence = append(NewSentence, sentence[i])
	}
	return NewSentence
}

func GenWordForTrain2(sentence []string, k int) []string {
	NewSentence := make([]string, 0)
	lastIndex := k + WordRight + 2
	firstIndex := k - WordLeft - 2

	if len(sentence)-1 < lastIndex {
		lastIndex = len(sentence) - 1
	}

	if firstIndex < 0 {
		firstIndex = 0
	}

	for i := firstIndex; i <= lastIndex; i++ {
		NewSentence = append(NewSentence, sentence[i])
	}

	return NewSentence
}

func CopyTokenSentence(sentence CrfTrainSentence, f int, l int) CrfTrainSentence {
	fx := 0
	lx := l

	if f > 0 {
		fx = f - 2
	}
	if len(sentence.Sentence)-1 > l {
		lx = l + 2
	}

	sen := CrfTrainSentence{}
	for i := fx; i <= lx; i++ {
		sen.Sentence = append(sen.Sentence, sentence.Sentence[i])
		sen.Label = append(sen.Label, sentence.Label[i])
	}

	return sen
}

func GenTokenSentenceForTrain(sentence CrfTrainSentence) ([]int, []int, []CrfTrainSentence) {
	tokenSentence := make([]CrfTrainSentence, 0)
	firstIndex := make([]int, 0)
	lastIndex := make([]int, 0)

	fIndex := 0
	lIndex := 0

	if len(sentence.Sentence) == 0 {
		firstIndex = append(firstIndex, 0)
		lastIndex = append(lastIndex, 0)
		tokenSentence = append(tokenSentence, CrfTrainSentence{})
		return firstIndex, lastIndex, tokenSentence
	}

	for {
		if lIndex == 0 {
			fIndex = 0
			if len(sentence.Sentence) > tokenSize+2 {
				lIndex = tokenSize - 1
				firstIndex = append(firstIndex, 0)
				lastIndex = append(lastIndex, lIndex)
				tokenSentence = append(tokenSentence, CopyTokenSentence(sentence, fIndex, lIndex))
			} else {
				lIndex = len(sentence.Sentence) - 1
				firstIndex = append(firstIndex, 0)
				lastIndex = append(lastIndex, lIndex)
				tokenSentence = append(tokenSentence, CopyTokenSentence(sentence, fIndex, lIndex))
				break
			}
		} else {
			fIndex = lIndex + 1
			if len(sentence.Sentence) > fIndex+tokenSize+2 {
				lIndex = fIndex + tokenSize - 1
				firstIndex = append(firstIndex, 2)
				lastIndex = append(lastIndex, tokenSize+1)
				tokenSentence = append(tokenSentence, CopyTokenSentence(sentence, fIndex, lIndex))
			} else {
				lIndex = len(sentence.Sentence) - 1
				firstIndex = append(firstIndex, 2)
				lastIndex = append(lastIndex, len(sentence.Sentence)-fIndex+1)
				tokenSentence = append(tokenSentence, CopyTokenSentence(sentence, fIndex, lIndex))
				break
			}
		}
	}
	return firstIndex, lastIndex, tokenSentence
}

func GenChunkForTrain(sentence ChunkCrfSentence, k int) ([]string, []string, []string) {
	NewSentence := make([]string, 0)
	NewLabel := make([]string, 0)
	NewChunk := make([]string, 0)

	lastIndex := k + 2
	firstIndex := k - 2
	if len(sentence.Sentence)-1 < lastIndex {
		lastIndex = len(sentence.Sentence) - 1
	}
	if k < 2 {
		firstIndex = 0
	}
	for i := firstIndex; i <= lastIndex; i++ {
		NewSentence = append(NewSentence, sentence.Sentence[i])
		NewLabel = append(NewLabel, sentence.Label[i])
		NewChunk = append(NewChunk, sentence.Chunk[i])
	}
	return NewSentence, NewLabel, NewChunk
}

func GenChunkForTrain2(sentence ChunkCrfSentence, k int) ([]string, []string, []string) {
	NewSentence := make([]string, 0)
	NewLabel := make([]string, 0)
	NewChunk := make([]string, 0)

	lastIndex := k + ChunkRight
	firstIndex := k - ChunkLeft
	if len(sentence.Sentence)-1 < lastIndex {
		lastIndex = len(sentence.Sentence) - 1
	}
	if k < ChunkLeft {
		firstIndex = 0
	}
	for i := firstIndex; i <= lastIndex; i++ {
		NewSentence = append(NewSentence, sentence.Sentence[i])
		NewLabel = append(NewLabel, sentence.Label[i])
		NewChunk = append(NewChunk, sentence.Chunk[i])
	}
	return NewSentence, NewLabel, NewChunk
}

func GenLabel(sentence []string, dict Words) [][]string {
	return GenLabelRecursive(sentence, 0, dict)
}

func GenLabelRecursive(sentence []string, id int, dict Words) [][]string {
	result := make([][]string, 0)
	if id >= len(sentence) {
		return result
	}
	restLabel := GenLabelRecursive(sentence, id+1, dict)
	//	if sentence[id][0] == '<' {
	if IsTag(sentence[id]) {
		labelSQ := make([]string, 0)
		labelSQ = append(labelSQ, sentence[id][1:len(sentence[id])-1])
		if len(restLabel) == 0 {
			result = append(result, labelSQ)
		} else {
			for kk := range restLabel {
				restLabelXX := append(labelSQ, restLabel[kk]...)
				result = append(result, restLabelXX)
			}
		}
		return result
	}

	pos, ok := dict[sentence[id]]

	if ok {
		for k := range pos {
			labelSQ := make([]string, 0)
			labelSQ = append(labelSQ, pos[k])

			if len(restLabel) == 0 {
				result = append(result, labelSQ)
			} else {
				for kk := range restLabel {
					restLabelXX := append(labelSQ, restLabel[kk]...)
					result = append(result, restLabelXX)
				}
			}
		}

	} else {
		tag := "UNK"

		if IsTag(sentence[id]) {
			tag = sentence[id][1 : len(sentence[id])-1]
		} else if IsNumber(sentence[id]) {
			tag = "NUM"
		}

		labelSQ := make([]string, 0)
		labelSQ = append(labelSQ, tag)

		if len(restLabel) == 0 {
			result = append(result, labelSQ)
		} else {
			for k := range restLabel {
				restLabelXX := append(labelSQ, restLabel[k]...)
				result = append(result, restLabelXX)
			}
		}
	}
	return result
}

func IsForeign1(data string) bool {
	r := regexp.MustCompile(`[a-z,A-Z]+`)
	return r.MatchString(data)
}

func IsNumber(data string) bool {
	r := regexp.MustCompile(`\d+`)
	return r.MatchString(data)
}

func IsTag(data string) bool {
	rdata := make([]string, 0)
	for _, v := range data {
		rdata = append(rdata, string(v))
	}

	if rdata[0] == "<" && rdata[len(rdata)-1] == ">" {
		for i := range rdata[1 : len(rdata)-2] {
			if rdata[i] == ">" {
				return false
			}
		}
		return true
	}
	return false
}

func findOpenAll(data string, ix int) int {
	ixx := ix
	for {
		if ixx >= len(data) {
			return -1
		} else if data[ixx] == '[' {
			return ixx
		} else if data[ixx] == '<' {
			return ixx
		}
		ixx++
	}
}

func findClose1(data string, ix int) int {
	ixx := ix
	count := 0
	for {
		if ixx >= len(data) {
			return -1
		} else if data[ixx] == '[' {
			count++
		} else if data[ixx] == ']' {
			if count == 0 {
				return ixx
			}
			count--
		}
		ixx++
	}
}

func findOpen2(data string, ix int) int {
	ixx := ix
	for {
		if ixx >= len(data) {
			return -1
		} else if data[ixx] == '<' {
			return ixx
		}
		ixx++
	}
}

func findClose2(data string, ix int) int {
	ixx := ix
	for {
		if ixx >= len(data) {
			return -1
		} else if data[ixx] == '>' {
			return ixx
		}
		ixx++
	}
}

func WordTagString(data CrfTrainSentence) string {
	out := ""
	for i := range data.Sentence {
		if data.Sentence[i] == "<"+data.Label[i]+">" {
			out += data.Sentence[i]
		} else {
			out += "[" + data.Sentence[i] + "]<" + data.Label[i] + ">"
		}
	}
	return out
}

func GetWordTag(data string) CrfTrainSentence {
	output := CrfTrainSentence{}
	output.Sentence = make([]string, 0)
	output.Label = make([]string, 0)

	ix := 0
	for ix < len(data) {
		b1 := findOpenAll(data, ix)
		if b1 == -1 {
			return output
		}
		if data[b1] == '[' {
			ix = b1 + 1
			b2 := findClose1(data, ix)
			if b2 == -1 {
				return output
			}
			ix = b2 + 1
			b3 := findOpen2(data, ix)
			if b3 == -1 {
				return output
			}
			ix = b3 + 1
			b4 := findClose2(data, ix)
			if b4 == -1 {
				return output
			}
			ix = b4 + 1

			sentence := data[b1+1 : b2]
			label := data[b3+1 : b4]

			output.Sentence = append(output.Sentence, sentence)
			output.Label = append(output.Label, label)

		} else if data[b1] == '<' {
			ix = b1 + 1
			b2 := findClose2(data, ix)
			if b2 == -1 {
				return output
			}
			ix = b2 + 1

			sentence := data[b1 : b2+1]
			label := data[b1+1 : b2]

			output.Sentence = append(output.Sentence, sentence)
			output.Label = append(output.Label, label)
		}
	}
	return output
}

func Feature1(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f1 = "start"
	} else {
		f1 = wordtag.Label[ix-1]
	}
	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature2(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f2 = "start"
		f1 = "start"
	} else if ix == 1 {
		f2 = "start"
		f1 = wordtag.Label[0]
	} else {
		f2 = wordtag.Label[ix-2]
		f1 = wordtag.Label[ix-1]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature3(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature4(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
		f2b = "stop"
	} else if ix == len(wordtag.Label)-2 {
		f1b = wordtag.Label[ix+1]
		f2b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
		f2b = wordtag.Label[ix+2]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature5(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f1 = "start"
	} else {
		f1 = wordtag.Label[ix-1]
	}

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature6(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f2 = "start"
		f1 = "start"
	} else if ix == 1 {
		f2 = "start"
		f1 = wordtag.Label[0]
	} else {
		f2 = wordtag.Label[ix-2]
		f1 = wordtag.Label[ix-1]
	}

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature7(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f1 = "start"
	} else {
		f1 = wordtag.Label[ix-1]
	}

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
		f2b = "stop"
	} else if ix == len(wordtag.Label)-2 {
		f1b = wordtag.Label[ix+1]
		f2b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
		f2b = wordtag.Label[ix+2]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature8(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f2 = "start"
		f1 = "start"
	} else if ix == 1 {
		f2 = "start"
		f1 = wordtag.Label[0]
	} else {
		f2 = wordtag.Label[ix-2]
		f1 = wordtag.Label[ix-1]
	}

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
		f2b = "stop"
	} else if ix == len(wordtag.Label)-2 {
		f1b = wordtag.Label[ix+1]
		f2b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
		f2b = wordtag.Label[ix+2]
	}

	return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
}

func Feature9(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f1 = "start"
	} else {
		f1 = wordtag.Label[ix-1]
	}

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
	}

	if wordtag.Label[ix] == "NUM" || wordtag.Label[ix] == "UNK" || IsTag(wordtag.Sentence[ix]) {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
	} else {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b + "#" + wordtag.Sentence[ix]
	}
}

func Feature10(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f1 = "start"
	} else {
		f1 = wordtag.Label[ix-1]
	}

	if wordtag.Label[ix] == "NUM" || wordtag.Label[ix] == "UNK" || IsTag(wordtag.Sentence[ix]) {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
	} else {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b + "#" + wordtag.Sentence[ix]
	}
}

func Feature11(wordtag CrfTrainSentence, ix int) string {
	f2 := "none"
	f1 := "none"
	f0 := wordtag.Label[ix]
	f1b := "none"
	f2b := "none"

	if ix == len(wordtag.Label)-1 {
		f1b = "stop"
	} else {
		f1b = wordtag.Label[ix+1]
	}

	if wordtag.Label[ix] == "NUM" || wordtag.Label[ix] == "UNK" || IsTag(wordtag.Sentence[ix]) {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b
	} else {
		return f2 + ":" + f1 + ":" + f0 + ":" + f1b + ":" + f2b + "#" + wordtag.Sentence[ix]
	}
}

func Feature1Chunk(wordtag ChunkCrfSentence, ix int) string {
	L1 := "none"
	L0 := wordtag.Chunk[ix]
	POS1 := "none"
	POS0 := wordtag.Label[ix]
	W1 := "none"
	W0 := "none"

	if ix == 0 {
		L1 = "start"
		POS1 = "start"
	} else {
		L1 = wordtag.Chunk[ix-1]
		POS1 = wordtag.Label[ix-1]
	}
	return L1 + ":" + L0 + ":" + POS1 + ":" + POS0 + ":" + W1 + ":" + W0
}

func Feature2Chunk(wordtag ChunkCrfSentence, ix int) string {
	L1 := "none"
	L0 := wordtag.Chunk[ix]
	POS1 := "none"
	POS0 := wordtag.Label[ix]
	W1 := "none"
	W0 := wordtag.Sentence[ix]
	if wordtag.Label[ix] == "NUM" || wordtag.Label[ix] == "UNK" {
		W0 = "none"
	}

	if ix == 0 {
		L1 = "start"
		POS1 = "start"
	} else {
		L1 = wordtag.Chunk[ix-1]
		POS1 = wordtag.Label[ix-1]
	}
	return L1 + ":" + L0 + ":" + POS1 + ":" + POS0 + ":" + W1 + ":" + W0
}

func Feature3Chunk(wordtag ChunkCrfSentence, ix int) string {
	L1 := "none"
	L0 := wordtag.Chunk[ix]
	POS1 := "none"
	POS0 := wordtag.Label[ix]
	W1 := "none"
	W0 := "none"

	if ix == 0 {
		L1 = "start"
		POS1 = "start"
		W1 = "start"
	} else {
		L1 = wordtag.Chunk[ix-1]
		POS1 = wordtag.Label[ix-1]
		W1 = wordtag.Sentence[ix-1]
		if wordtag.Label[ix-1] == "NUM" || wordtag.Label[ix-1] == "UNK" {
			W1 = "none"
		}

	}
	return L1 + ":" + L0 + ":" + POS1 + ":" + POS0 + ":" + W1 + ":" + W0
}

func Feature4Chunk(wordtag ChunkCrfSentence, ix int) string {
	L1 := "none"
	L0 := wordtag.Chunk[ix]
	POS1 := "none"
	POS0 := wordtag.Label[ix]
	W1 := "none"
	W0 := wordtag.Sentence[ix]

	if ix == 0 {
		L1 = "start"
		POS1 = "start"
		W1 = "start"
	} else {
		L1 = wordtag.Chunk[ix-1]
		POS1 = wordtag.Label[ix-1]
		W1 = wordtag.Sentence[ix-1]
	}
	return L1 + ":" + L0 + ":" + POS1 + ":" + POS0 + ":" + W1 + ":" + W0
}

func ExtractFeature(data []string) []string {
	output := make(map[string]bool)

	for kx := range data {
		lines := strings.Split(data[kx], "\n")
		for k := range lines {
			wordTagx := GetWordTag(lines[k])
			for kk := range wordTagx.Label {

				f1 := Feature1(wordTagx, kk)
				output[f1] = true

				f2 := Feature2(wordTagx, kk)
				output[f2] = true

				f3 := Feature3(wordTagx, kk)
				output[f3] = true

				f4 := Feature4(wordTagx, kk)
				output[f4] = true

				f5 := Feature5(wordTagx, kk)
				output[f5] = true

				f6 := Feature6(wordTagx, kk)
				output[f6] = true

				f7 := Feature7(wordTagx, kk)
				output[f7] = true

				f8 := Feature8(wordTagx, kk)
				output[f8] = true

				f9 := Feature9(wordTagx, kk)
				output[f9] = true

				f10 := Feature10(wordTagx, kk)
				output[f10] = true

				f11 := Feature11(wordTagx, kk)
				output[f11] = true

			}
		}
	}

	result := make([]string, 0)
	for k := range output {
		result = append(result, k)
	}
	return result
}

func ExtractChunkFeature(data []string) []string {
	output := make(map[string]bool)

	for kx := range data {
		lines := strings.Split(data[kx], "\n")
		for k := range lines {
			wordTagxZero := GetWordTag(lines[k])
			wordTagx := MakeChunkInfo(wordTagxZero)
			for kk := range wordTagx.Label {

				f1 := Feature1Chunk(wordTagx, kk)
				output[f1] = true

				f2 := Feature2Chunk(wordTagx, kk)
				output[f2] = true

				f3 := Feature3Chunk(wordTagx, kk)
				output[f3] = true

				//				f4 := Feature4Chunk(wordTagx, kk)
				//				output[f4] = true
			}
		}
	}

	result := make([]string, 0)
	for k := range output {
		result = append(result, k)
	}
	return result
}

func MakeChunkInfo(words CrfTrainSentence) ChunkCrfSentence {
	output := ChunkCrfSentence{}
	output.Sentence = make([]string, 0)
	output.Label = make([]string, 0)
	output.Chunk = make([]string, 0)

	for k := range words.Sentence {
		chunk, tag := ExtractChunkFromPOS(words.Label[k])
		if IsTag(words.Sentence[k]) {
			//if words.Sentence[k][0] == '<' {
			output.Sentence = append(output.Sentence, "<"+tag+">")
		} else {
			output.Sentence = append(output.Sentence, words.Sentence[k])
		}
		output.Label = append(output.Label, tag)
		output.Chunk = append(output.Chunk, chunk)
	}

	return output
}

func ExtractChunkFromPOS(label string) (string, string) {
	if label[0] == '*' {
		if label[1] == '*' {
			return "**", label[2:]
		} else {
			return "*", label[1:]
		}
	} else if label[0] == '+' {
		if label[1] == '+' {
			return "++", label[2:]
		} else {
			return "+", label[1:]
		}
	} else if label[0] == '$' {
		if label[1] == '$' {
			return "$$", label[2:]
		} else {
			return "$", label[1:]
		}
	} else if label[0] == '#' {
		if label[1] == '#' {
			return "##", label[2:]
		} else {
			return "#", label[1:]
		}
	} else if label[0] == '@' {
		if label[1] == '@' {
			return "@@", label[2:]
		} else {
			return "@", label[1:]
		}
	} else if label[0] == '%' {
		if label[1] == '%' {
			return "%%", label[2:]
		} else {
			return "%", label[1:]
		}
	} else if label[0] == '&' {
		if label[1] == '&' {
			return "&&", label[2:]
		} else {
			return "&", label[1:]
		}
	} else if label[0] == '=' {
		if label[1] == '=' {
			return "==", label[2:]
		} else {
			return "=", label[1:]
		}
	} else if label[0] == '?' {
		if label[1] == '?' {
			return "??", label[2:]
		} else {
			return "?", label[1:]
		}
	} else if label[0] == '-' {
		if label[1] == '-' {
			return "--", label[2:]
		} else {
			return "-", label[1:]
		}
	} else if label[0] == '!' {
		if label[1] == '!' {
			return "!!", label[2:]
		} else {
			return "!", label[1:]
		}
	}

	return label, label
}

func NormalizeSentence(wordtag CrfTrainSentence) CrfTrainSentence {
	result := CrfTrainSentence{}
	result.Sentence = make([]string, 0)
	result.Label = make([]string, 0)

	for k := range wordtag.Sentence {
		if wordtag.Sentence[k] == "<space>" {
			if k == 0 {
				continue
			} else if k == len(wordtag.Sentence)-1 {
				continue
			} else if IsTag(wordtag.Sentence[k-1]) {
				continue
			} else if IsTag(wordtag.Sentence[k+1]) {
				continue
			} else if wordtag.Sentence[k+1] == "ๆ" {
				continue
			} else if wordtag.Sentence[k-1] == "ๆ" {
				if k+2 < len(wordtag.Sentence) && wordtag.Sentence[k+2] == "ๆ" {
					continue
				}
				if k+3 < len(wordtag.Sentence) && wordtag.Sentence[k+2] == "<space>" && wordtag.Sentence[k+3] == "ๆ" {
					continue
				}
			} else if IsNumber(wordtag.Sentence[k-1]) {
				continue
			} else if IsNumber(wordtag.Sentence[k+1]) {
				continue
			}

		}
		result.Sentence = append(result.Sentence, wordtag.Sentence[k])
		result.Label = append(result.Label, wordtag.Label[k])

	}
	return result
}

func CutSpace(wordtag CrfTrainSentence) []CrfTrainSentence {
	result := make([]CrfTrainSentence, 0)
	local := CrfTrainSentence{}
	local.Sentence = make([]string, 0)
	local.Label = make([]string, 0)
	idx := 0
	for {
		if idx >= len(wordtag.Sentence) {
			result = append(result, local)
			break
		}
		if wordtag.Sentence[idx] == "<space>" {
			if len(wordtag.Sentence) > idx+1 && IsForeign1(wordtag.Sentence[idx-1]) && IsForeign1(wordtag.Sentence[idx+1]) {
				local.Sentence = append(local.Sentence, wordtag.Sentence[idx])
				local.Label = append(local.Label, wordtag.Label[idx])
			} else {
				result = append(result, local)
				local = CrfTrainSentence{}
				local.Sentence = make([]string, 0)
				local.Label = make([]string, 0)
			}
		} else {
			local.Sentence = append(local.Sentence, wordtag.Sentence[idx])
			local.Label = append(local.Label, wordtag.Label[idx])
		}
		idx++
	}
	return result
}

func LoadFeature() (result []CrfFeature) {
	result = make([]CrfFeature, 0)
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `c`,`w` from `crf`;")
	rows, err := st.Query()
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		cdat := ""
		wdat := float64(0)
		rows.Scan(&cdat, &wdat)
		cdatX := strings.Split(cdat, "#")
		cdatx := strings.Split(cdatX[0], ":")
		featureData := CrfFeature{
			Feature2:  cdatx[0],
			Feature1:  cdatx[1],
			Feature0:  cdatx[2],
			Feature1b: cdatx[3],
			Feature2b: cdatx[4],
			Word:      "",
			W:         wdat,
		}
		if len(cdatX) > 1 {
			featureData.Word = cdatX[1]
		}
		result = append(result, featureData)
	}
	return
}

func LoadChunkFeature() (result []ChunkCrfFeature) {
	result = make([]ChunkCrfFeature, 0)
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `c`,`w` from `chunkcrf`;")
	rows, err := st.Query()
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		cdat := ""
		wdat := float64(0)
		rows.Scan(&cdat, &wdat)
		cdatx := strings.Split(cdat, ":")
		featureData := ChunkCrfFeature{
			L1:   cdatx[0],
			L0:   cdatx[1],
			POS1: cdatx[2],
			POS0: cdatx[3],
			W1:   cdatx[4],
			W0:   cdatx[5],
			W:    wdat,
		}
		result = append(result, featureData)
	}
	return
}

func GetScoreCRF(label []string, sentence []string, features []CrfFeature) (score float64) {
	score = 0
	for k := range features {
		for kk := range label {
			score += GetFeatureScore(k, features, kk, label, sentence) * features[k].W
		}
	}
	return
}

func GetScoreCRF2(label []string, sentence []string, kx int, features []CrfFeature) (score float64) {
	score = 0
	for k := range features {
		score += GetFeatureScore(k, features, kx, label, sentence) * features[k].W
	}
	return
}

func GetScoreCRF3(label []string, sentence []string, features []CrfFeature, firstIndex int, lastIndex int) (score float64) {
	score = 0
	for k := range features {
		for kk := firstIndex; kk <= lastIndex; kk++ {
			score += GetFeatureScore(k, features, kk, label, sentence) * features[k].W
		}
	}
	return
}

func GetScoreChunkCRF(chunk []string, sentence []string, label []string, kx int, features []ChunkCrfFeature) (score float64) {
	score = 0
	for k := range features {
		score += GetFeatureChunkScore(k, features, kx, chunk, sentence, label) * features[k].W
	}
	return
}

func GetScoreChunkCRFx(chunk []string, sentence []string, label []string, kx int, features []ChunkCrfFeature) (score float64) {
	score = 0
	for k := range features {
		for kk := kx; kk < len(chunk); kk++ {
			score += GetFeatureChunkScore(k, features, kk, chunk, sentence, label) * features[k].W
		}
	}
	return
}

func GetScoreCRF2x(label []string, sentence []string, features []CrfFeature, restData int) (score float64) {
	score = 0
	startIndex := 0
	lastIndex := 0

	if restData == 0 {
		lastIndex = len(label)
	} else {
		lastIndex = len(label) - 2
	}
	if len(label) == len(sentence) {
		startIndex = 0
	} else {
		startIndex = 2
	}

	for k := range features {
		for kk := startIndex; kk < lastIndex; kk++ {
			score += GetFeatureScore2(k, features, kk, label, sentence, startIndex) * features[k].W
		}
	}
	return
}

func GetScoreCRF2x2(label []string, sentence []string, features []CrfFeature, restData int) (score float64) {
	score = 0
	startIndex := 0
	lastIndex := 0

	if restData == 0 {
		lastIndex = len(label)
	} else {
		lastIndex = len(label) - 2
	}

	startIndex = len(label) - len(sentence)

	for k := range features {
		for kk := startIndex; kk < lastIndex; kk++ {
			score += GetFeatureScore2(k, features, kk, label, sentence, startIndex) * features[k].W
		}
	}
	return
}

func GetFeatureScore(k int, features []CrfFeature, kk int, label []string, sentence []string) (score float64) {
	featurex := FeatureX(kk, label)

	if features[k].Feature2 != "none" {
		if features[k].Feature2 != featurex.Feature2 {
			return 0
		}
	}

	if features[k].Feature1 != "none" {
		if features[k].Feature1 != featurex.Feature1 {
			return 0
		}
	}

	if features[k].Feature2b != "none" {
		if features[k].Feature2b != featurex.Feature2b {
			return 0
		}
	}

	if features[k].Feature1b != "none" {
		if features[k].Feature1b != featurex.Feature1b {
			return 0
		}
	}

	if features[k].Feature0 != featurex.Feature0 {
		return 0
	}

	if features[k].Word != "" {
		if features[k].Word != sentence[kk] {
			return 0
		}
	}

	return 1
}

func GetFeatureScore2(k int, features []CrfFeature, kk int, label []string, sentence []string, startIndex int) (score float64) {
	featurex := FeatureX(kk, label)

	if features[k].Feature2 != "none" {
		if features[k].Feature2 != featurex.Feature2 {
			return 0
		}
	}

	if features[k].Feature1 != "none" {
		if features[k].Feature1 != featurex.Feature1 {
			return 0
		}
	}

	if features[k].Feature2b != "none" {
		if features[k].Feature2b != featurex.Feature2b {
			return 0
		}
	}

	if features[k].Feature1b != "none" {
		if features[k].Feature1b != featurex.Feature1b {
			return 0
		}
	}

	if features[k].Feature0 != featurex.Feature0 {
		return 0
	}

	if features[k].Word != "" {
		if features[k].Word != sentence[kk-startIndex] {
			return 0
		}
	}

	return 1
}

func GetFeatureChunkScore(k int, features []ChunkCrfFeature, kk int, chunk []string, sentence []string, label []string) (score float64) {
	featurex := FeatureXChunk(kk, chunk, sentence, label)

	if features[k].L1 != featurex.L1 {
		return 0
	}

	if features[k].L0 != featurex.L0 {
		return 0
	}

	if features[k].POS1 != "none" && features[k].POS1 != featurex.POS1 {
		return 0
	}

	if features[k].POS0 != featurex.POS0 {
		return 0
	}

	if features[k].W1 != "none" && features[k].W1 != featurex.W1 {
		return 0
	}

	if features[k].W0 != "none" && features[k].W0 != featurex.W0 {
		return 0
	}

	return 1
}

func FeatureX(ix int, label []string) CrfFeature {
	f2 := "none"
	f1 := "none"
	f0 := label[ix]
	f1b := "none"
	f2b := "none"

	if ix == 0 {
		f2 = "start"
		f1 = "start"
	} else if ix == 1 {
		f2 = "start"
		f1 = label[0]
	} else {
		f2 = label[ix-2]
		f1 = label[ix-1]
	}

	if ix == len(label)-1 {
		f1b = "stop"
		f2b = "stop"
	} else if ix == len(label)-2 {
		f1b = label[ix+1]
		f2b = "stop"
	} else {
		f1b = label[ix+1]
		f2b = label[ix+2]
	}

	return CrfFeature{Feature2: f2, Feature1: f1, Feature0: f0, Feature1b: f1b, Feature2b: f2b}
}

func FeatureXChunk(ix int, chunk []string, sentence []string, label []string) ChunkCrfFeature {
	L1 := "none"
	L0 := chunk[ix]
	POS1 := "none"
	POS0 := label[ix]
	W1 := "none"
	W0 := sentence[ix]

	if ix == 0 {
		L1 = "start"
		POS1 = "start"
		W1 = "start"
	} else {
		L1 = chunk[ix-1]
		POS1 = label[ix-1]
		W1 = sentence[ix-1]
	}

	return ChunkCrfFeature{L1: L1, L0: L0, POS1: POS1, POS0: POS0, W1: W1, W0: W0}
}
func FindSpace(data string, start int) int {
	startID := start
	for {
		s1 := findOpen2(data, startID)
		if s1 == -1 {
			return -1
		}

		s2 := findClose2(data, s1)
		if s2 == -1 {
			return -1
		}
		tag := data[s1+1 : s2]
		if tag == "space" {
			return s1
		}
		startID = s2
	}

	return -1
}

func RemoveSpace(data string) string {
	result := ""
	d := strings.Split(data, " ")
	for k := range d {
		result += d[k]
	}
	return result
}

func NormalizeLineX(datax string) string {
	data := RemoveSpace(datax)
	idx := 0
	result := ""
	for {
		spx := FindSpace(data, idx)
		if spx == -1 {
			result += data[idx:len(data)]
			break
		}

		if len(data) <= spx+7 {
			result += data[idx:spx]
			break
		}

		if spx == 0 {
			idx = 7
			continue
		}

		result += data[idx:spx]

		if data[spx-1] == '>' {
			idx = spx + 7
			continue
		}

		if data[spx+7] == '<' {
			idx = spx + 7
			continue
		}

		if IsNumber(string(data[spx-1])) {
			idx = spx + 7
			continue
		}

		if IsNumber(string(data[spx+7])) {
			idx = spx + 7
			continue
		}

		if IsForeign1(string(data[spx-1])) {
			if IsForeign1(string(data[spx+7])) {
				result += "<space>"
				idx = spx + 7
				continue
			}
			idx = spx + 7
			continue
		}

		if IsForeign1(string(data[spx+7])) {
			idx = spx + 7
			continue
		}

		if len(data) >= spx+10 {
			if data[spx+7:spx+10] == "ๆ" {
				idx = spx + 7
				continue
			}
		}

		if data[spx-3:spx] == "ๆ" {
			idx = spx + 7
			continue
		}

		result += "<space>"
		idx = spx + 7

	}
	return result
}

func NormalizeLine(data string) string {
	lines := strings.Split(data, "\n")
	linesX := make([]string, 0)
	for k := range lines {
		lineNormal := NormalizeLineX(lines[k])
		linesX = append(linesX, lineNormal)
	}

	result := ""
	for k := range linesX {
		result += linesX[k] + "\n"
	}
	return result
}

func SplitWithSPChar(data string) []string {
	datax := data
	result := make([]string, 0, 0)
	for {
		r := regexp.MustCompile(`<[^>]+>`)
		loc := r.FindStringIndex(datax)
		if loc == nil {
			if datax != "" {
				result = append(result, datax)
			}
			break
		}
		d1 := datax[:loc[0]]
		d2 := datax[loc[0]:loc[1]]
		d3 := datax[loc[1]:]

		if d1 != "" {
			result = append(result, d1)
		}
		result = append(result, d2)
		datax = d3
	}
	return result
}

func EvalTag(data string, dict Words, features []CrfFeature) (float64, []string, string) {
	datax := strings.Split(data, " ")
	postags := GenLabel(datax, dict)
	score := make([]float64, len(postags))
	prob := make([]float64, len(postags))
	z := float64(0)

	for k := range postags {
		score[k] = GetScoreCRF(postags[k], datax, features)
		z += math.Exp(score[k])
	}

	for k := range postags {
		prob[k] = math.Exp(score[k]) / z
	}

	best := 0
	for k := range postags {
		if prob[k] > prob[best] {
			best = k
		}
	}

	result := ""
	for k := range datax {
		if IsTag(datax[k]) {
			result += datax[k]
		} else {
			result += "[" + datax[k] + "]<" + postags[best][k] + ">"
		}
	}

	return prob[best], postags[best], result[:len(result)]
}

func EvalTag2(data string, dict Words, features []CrfFeature, preTags []string, restData int) (float64, float64, []string, string, string) {
	fmt.Printf("Data = %s\nPreTags = ", data)
	for i := range preTags {
		fmt.Printf("%s ", preTags[i])
	}
	fmt.Printf("\nrestData = %d\n", restData)

	datax := strings.Split(data, " ")
	postagsX := GenLabel(datax, dict)
	postags := make([][]string, 0)
	if len(preTags) == 0 {
		postags = postagsX
	} else {
		for i := range postagsX {
			postagsQ := make([]string, 0)
			postagsQ = append(postagsQ, preTags...)
			postagsQ = append(postagsQ, postagsX[i]...)
			postags = append(postags, postagsQ)
		}
	}
	score := make([]float64, len(postags))
	prob := make([]float64, len(postags))
	z := float64(0)

	for k := range postags {
		score[k] = GetScoreCRF2x(postags[k], datax, features, restData)
		for i := range postags[k] {
			fmt.Printf("%s ", postags[k][i])
		}
		fmt.Printf("  score = %f\n", score[k])

		z += math.Exp(score[k])
	}

	for k := range postags {
		prob[k] = math.Exp(score[k]) / z
	}

	best := 0
	for k := range postags {
		if prob[k] > prob[best] {
			best = k
		}
	}

	result := ""
	textUsed := ""
	lastIndex := 0
	postagOutput := make([]string, 0)

	if restData == 0 {
		lastIndex = len(postagsX[best])
		postagOutput = postagsX[best]
	} else {
		lastIndex = len(postagsX[best]) - 2
		postagOutput = postagsX[best][:len(postagsX[best])-2]
	}

	for k := 0; k < lastIndex; k++ {
		if IsTag(datax[k]) {
			result += datax[k]
			textUsed += datax[k]
		} else {
			result += "[" + datax[k] + "]<" + postagsX[best][k] + ">"
			textUsed += datax[k]
		}
	}

	return score[best], prob[best], postagOutput, result[:len(result)], textUsed
}

func EvalTag3(data string, dict Words, features []CrfFeature, preTags []string, restData int) (float64, float64, []string, string, string) {
	//	fmt.Printf("Data = %s\nPreTags = ", data)
	//	for i := range preTags {
	//		fmt.Printf("%s ", preTags[i])
	//	}
	//	fmt.Printf("\nrestData = %d\n", restData)

	datax := strings.Split(data, " ")
	postagsX := GenLabel(datax, dict)
	postags := make([][]string, 0)
	if len(preTags) == 0 {
		postags = postagsX
	} else {
		for i := range postagsX {
			postagsQ := make([]string, 0)
			postagsQ = append(postagsQ, preTags...)
			postagsQ = append(postagsQ, postagsX[i]...)
			postags = append(postags, postagsQ)
		}
	}
	score := make([]float64, len(postags))
	prob := make([]float64, len(postags))
	z := float64(0)

	for k := range postags {
		score[k] = GetScoreCRF2x2(postags[k], datax, features, restData)
		//		for i := range postags[k] {
		//			fmt.Printf("%s ", postags[k][i])
		//		}
		//		fmt.Printf("  score = %f\n", score[k])

		z += math.Exp(score[k])
	}

	for k := range postags {
		prob[k] = math.Exp(score[k]) / z
	}

	best := 0
	for k := range postags {
		if prob[k] > prob[best] {
			best = k
		}
	}

	result := ""
	textUsed := ""
	lastIndex := 0
	postagOutput := make([]string, 0)

	if restData == 0 {
		lastIndex = len(postagsX[best])
		postagOutput = postagsX[best]
	} else {
		lastIndex = len(postagsX[best]) - 2
		postagOutput = postagsX[best][:len(postagsX[best])-2]
	}

	for k := 0; k < lastIndex; k++ {
		if IsTag(datax[k]) {
			result += datax[k]
			textUsed += datax[k]
		} else {
			result += "[" + datax[k] + "]<" + postagsX[best][k] + ">"
			textUsed += datax[k]
		}
	}

	return score[best], prob[best], postagOutput, result[:len(result)], textUsed
}

func FindLessUnknown(data []WordSegResult) []WordSegResult {
	result := make([]WordSegResult, 0, 0)
	min := data[0].Unknown
	for _, v := range data {
		if min > v.Unknown {
			min = v.Unknown
		}
	}
	for _, v := range data {
		if v.Unknown == min {
			result = append(result, v)
		}
	}
	return result
}

func FindLessUnknown2(data []WordSegResult2) []WordSegResult2 {
	result := make([]WordSegResult2, 0, 0)
	min := data[0].Unknown
	for _, v := range data {
		if min > v.Unknown {
			min = v.Unknown
		}
	}
	for _, v := range data {
		if v.Unknown == min {
			result = append(result, v)
		}
	}
	return result
}

func FindWordMatch(data string, dict Words) (string, []WordSegMatch) {
	datax := data
	wordMatch := make([]WordSegMatch, 0, 0)
	unknownWord := ""
	for {
		if datax == "" {
			break
		}

		if datax[0] == '<' {
			tag := ""
			next := 0
			for k := range datax {
				if datax[k] == '>' {
					tag = datax[0 : k+1]
					next = k + 1
					break
				}
			}
			wordMatch = append(wordMatch, WordSegMatch{MatchWord: tag, RestData: datax[next:]})
		} else {
			for k := range dict {
				if len(k) <= len(datax) {
					if k == datax[:len(k)] {
						wordMatch = append(wordMatch, WordSegMatch{MatchWord: k, RestData: datax[len(k):]})
					}
				}
			}
		}

		if len(wordMatch) == 0 {
			for _, v := range datax {
				buf := make([]byte, 10)
				n := utf8.EncodeRune(buf, v)
				unknownWord = unknownWord + datax[:n]
				datax = datax[n:]
				break
			}
		} else {
			break
		}
	}

	return unknownWord, wordMatch
}

func FindWordMatch2(dataIN string, dict Words) (string, []WordSegMatch, string) {
	unknown := ""
	data := dataIN
	output := make([]WordSegMatch, 0)
	for {
		unknownX := len(unknown)
		if data == "" {
			return unknown, output, dataIN[unknownX:]
		}

		for i := range dict {
			if len(i) <= len(data) {
				if i == data[:len(i)] {
					output = append(output, WordSegMatch{MatchWord: i, RestData: data[len(i):]})
				}
			}
		}

		if len(output) > 0 {
			return unknown, output, dataIN[unknownX:]
		}

		if data[0] == '<' {
			for k := range data {
				if data[k] == '>' {
					output = append(output, WordSegMatch{MatchWord: data[:k+1], RestData: data[k+1:]})
					return unknown, output, dataIN[unknownX:]
				}
			}
		}

		if IsNumber(string(data[0])) {
			for i, v := range data {
				if !IsNumber(string(v)) {
					output := append(output, WordSegMatch{MatchWord: data[:i], RestData: data[i:]})
					return unknown, output, dataIN[unknownX:]
				}
			}
		}

		for _, v := range data {
			buf := make([]byte, 10)
			n := utf8.EncodeRune(buf, v)
			unknown = unknown + data[:n]
			data = data[n:]
			break
		}
	}
}

func WordSegmentRecursive(data string, dict Words) []WordSegResult {
	if data == "" {
		return []WordSegResult{WordSegResult{Unknown: 0, Data: ""}}
	}

	unknownWord, wordMatch := FindWordMatch(data, dict)

	if len(wordMatch) == 0 {
		return []WordSegResult{WordSegResult{Unknown: 1, Data: unknownWord}}
	}

	wordSegResultData := make([]WordSegResult, 0, 0)

	for _, v := range wordMatch {
		wordSegResultPart := WordSegmentRecursive(v.RestData, dict)
		for _, vv := range wordSegResultPart {
			if vv.Data == "" {
				if unknownWord == "" {
					wordSegResultData = append(wordSegResultData, WordSegResult{Unknown: vv.Unknown, Data: v.MatchWord})
				} else {
					wordSegResultData = append(wordSegResultData, WordSegResult{Unknown: vv.Unknown + 1, Data: unknownWord + " " + v.MatchWord})
				}
			} else {
				if unknownWord == "" {
					wordSegResultData = append(wordSegResultData, WordSegResult{Unknown: vv.Unknown, Data: v.MatchWord + " " + vv.Data})
				} else {
					wordSegResultData = append(wordSegResultData, WordSegResult{Unknown: vv.Unknown + 1, Data: unknownWord + " " + v.MatchWord + " " + vv.Data})
				}
			}

		}
	}

	return wordSegResultData
}

func WordSegmentRecursive2(data string, dict Words, count int) []WordSegResult2 {
	if count == 0 {
		return []WordSegResult2{WordSegResult2{Unknown: 0, Data: "", RestData: data}}
	}
	if data == "" {
		return []WordSegResult2{WordSegResult2{Unknown: 0, Data: "", RestData: data}}
	}

	unknownWord, wordMatch, restUnknown := FindWordMatch2(data, dict)

	output := make([]WordSegResult2, 0)

	if unknownWord != "" {
		if restUnknown == "" {
			output = append(output, WordSegResult2{Unknown: 1, Data: unknownWord, RestData: ""})
			return output
		} else {
			WordSegResultPart := WordSegmentRecursive2(restUnknown, dict, count-1)
			for _, v := range WordSegResultPart {
				output = append(output, WordSegResult2{Unknown: v.Unknown + 1, Data: strings.TrimSpace(unknownWord + " " + v.Data), RestData: v.RestData})
			}
			return output
		}
	}

	for _, v := range wordMatch {
		WordSegResultPart := WordSegmentRecursive2(v.RestData, dict, count-1)
		for _, vx := range WordSegResultPart {
			output = append(output, WordSegResult2{Unknown: vx.Unknown, Data: strings.TrimSpace(v.MatchWord + " " + vx.Data), RestData: vx.RestData})
		}
	}
	return output
}

func findLessPosFalse(postagX [][]string, features []CrfFeature, wordseg []string, sentenceTagX []int) int {
	scorepos := make([]float64, len(postagX))
	for k := range postagX {
		score := float64(0)
		for kk := range postagX[k] {
			sentence := strings.Split(wordseg[sentenceTagX[k]], " ")
			for kkk := range features {
				score += GetFeatureScore(kkk, features, kk, postagX[k], sentence) * features[kkk].W
			}
		}
		fmt.Printf("score=%f - %s\n", score, wordseg[sentenceTagX[k]])
		scorepos[k] = score
	}
	best := 0
	for k := range scorepos {
		if scorepos[k] > scorepos[best] {
			best = k
		}
	}

	fmt.Printf("\n")

	return best
}

func WordSegment(data string) string {
	if data == "" {
		return ""
	}

	dict, err := GetDictDB()
	if err != nil {
		return ""
	}

	features := LoadFeature()

	wordseg := ""
	wseg := CutSpaceLine(data)
	for k := range wseg {
		wordsegx := FindLessUnknown(WordSegmentRecursive(wseg[k], dict))
		wordseg2 := make([]string, 0)
		for kk := range wordsegx {
			wordseg2 = append(wordseg2, wordsegx[kk].Data)
		}

		result := make([]string, 0)
		postagX := make([][]string, 0)
		sentenceTagX := make([]int, 0)
		for kk := range wordseg2 {
			probx, postag, resultx := EvalTag(wordseg2[kk], dict, features)
			postagX = append(postagX, postag)
			sentenceTagX = append(sentenceTagX, kk)
			result = append(result, resultx)
			fmt.Printf("prob=%f - %s\n", probx, resultx)
		}
		fmt.Printf("\n")

		best := findLessPosFalse(postagX, features, wordseg2, sentenceTagX)

		if k == len(wseg)-1 {
			wordseg += result[best]
		} else {
			wordseg += result[best] + "<space>"
		}
	}
	return StringWordTag(NormalizeSentence(GetWordTag(wordseg)))
}

func WordSegment2(data string) string {
	if data == "" {
		return ""
	}

	dict, err := GetDictDB()
	if err != nil {
		return ""
	}

	features := LoadFeature()

	wordseg := ""
	preTags := make([]string, 0)
	wseg := data
	for {
		wordsegx := FindLessUnknown2(WordSegmentRecursive2(wseg, dict, tokenSize+2))
		fmt.Printf("\n\nsize = %d\n\n\n", len(wordsegx))
		wordseg2 := make([]string, 0)
		restDatax := make([]string, 0)
		for kk := range wordsegx {
			wordseg2 = append(wordseg2, wordsegx[kk].Data)
			restDatax = append(restDatax, wordsegx[kk].RestData)
		}

		for i := range wordseg2 {
			fmt.Printf("%s - %s\n\n", wordseg2[i], restDatax[i])
		}
		fmt.Printf("\n")

		result := make([]string, 0)
		postagX := make([][]string, 0)
		sentenceTagX := make([]int, 0)
		scoreX := make([]float64, 0)
		textUsedX := make([]string, 0)

		for kk := range wordseg2 {
			score, probx, postag, resultx, textUsed := EvalTag2(wordseg2[kk], dict, features, preTags, len(restDatax[kk]))
			postagX = append(postagX, postag)
			sentenceTagX = append(sentenceTagX, kk)
			result = append(result, resultx)
			scoreX = append(scoreX, score)
			textUsedX = append(textUsedX, textUsed)

			fmt.Printf("prob=%f - %s - %s\n", probx, resultx, textUsed)
			for i := range postag {
				fmt.Printf("%s ", postag[i])
			}
			fmt.Printf("  score = %f\n\n\n", scoreX[kk])
		}
		fmt.Printf("\n")

		best := 0
		scoreBest := scoreX[0]
		for i := range scoreX {
			if scoreX[i] > scoreBest {
				best = i
			}
		}

		fmt.Printf("Selected:\n%s\nScore = %f\n\n", result[best], scoreX[best])

		wordseg += result[best]
		wseg = wseg[len(textUsedX[best]):]
		if wseg == "" {
			break
		}
		preTags = postagX[best][tokenSize-2 : tokenSize]
	}

	return StringWordTag(GetWordTag(wordseg))
}

func WordSegment3(data string) string {
	if data == "" {
		return ""
	}

	dict, err := GetDictDB()
	if err != nil {
		return ""
	}

	features := LoadFeature()

	wordseg := ""
	preTags := make([]string, 0)
	wseg := data
	//	fmt.Printf("Segmenting - %s\n\n", data)
	for {
		wordsegx := FindLessUnknown2(WordSegmentRecursive2(wseg, dict, tokenSize+2))
		//		fmt.Printf("\n\nsize = %d\n\n\n", len(wordsegx))
		wordseg2 := make([]string, 0)
		restDatax := make([]string, 0)
		for kk := range wordsegx {
			wordseg2 = append(wordseg2, wordsegx[kk].Data)
			restDatax = append(restDatax, wordsegx[kk].RestData)
		}

		//		for i := range wordseg2 {
		//			fmt.Printf("%s - %s\n\n", wordseg2[i], restDatax[i])
		//		}
		//		fmt.Printf("\n")

		result := make([]string, 0)
		postagX := make([][]string, 0)
		sentenceTagX := make([]int, 0)
		scoreX := make([]float64, 0)
		textUsedX := make([]string, 0)

		for kk := range wordseg2 {
			score, _, postag, resultx, textUsed := EvalTag3(wordseg2[kk], dict, features, preTags, len(restDatax[kk]))
			postagX = append(postagX, postag)
			sentenceTagX = append(sentenceTagX, kk)
			result = append(result, resultx)
			scoreX = append(scoreX, score)
			textUsedX = append(textUsedX, textUsed)

			//			fmt.Printf("prob=%f - %s - %s\n", probx, resultx, textUsed)
			//			for i := range postag {
			//				fmt.Printf("%s ", postag[i])
			//			}
			//			fmt.Printf("  score = %f\n\n\n", scoreX[kk])
		}
		//		fmt.Printf("\n")

		best := 0
		scoreBest := scoreX[0]
		for i := range scoreX {
			if scoreX[i] > scoreBest {
				best = i
			}
		}

		//		fmt.Printf("Selected:\n%s\nScore = %f\n\n", result[best], scoreX[best])

		resultxx := GetWordTag(result[best])
		resultxx2 := CrfTrainSentence{}
		resultxx2.Sentence = append(resultxx2.Sentence, resultxx.Sentence[0])
		resultxx2.Label = append(resultxx2.Label, resultxx.Label[0])

		wordseg += StringWordTag(resultxx2)
		wseg = wseg[len(resultxx2.Sentence[0]):]

		//	fmt.Printf("[%s]<%s>", resultxx2.Sentence[0], resultxx2.Label[0])

		if wseg == "" {
			break
		}
		if len(preTags) < 2 {
			preTags = append(preTags, resultxx2.Label[0])
		} else {
			preTags = preTags[1:]
			preTags = append(preTags, resultxx2.Label[0])
		}
	}
	//	fmt.Printf("\n\n")

	return StringWordTag(GetWordTag(wordseg))
}

func Retag(data string) string {
	if data == "" {
		return ""
	}

	wtag := GetWordTag(string(data))

	dict, err := GetDictDB()
	if err != nil {
		return ""
	}

	features := LoadFeature()

	wordseg := ""
	preTags := make([]string, 0)
	for ix := range wtag.Sentence {
		last := ix + tokenSize + 2
		rest := 1
		if last > len(wtag.Sentence) {
			last = len(wtag.Sentence)
			rest = 0
		}
		wsegx := ""
		for im := ix; im < last; im++ {
			switch im {
			case ix:
				wsegx = wtag.Sentence[im]
			default:
				wsegx += " " + wtag.Sentence[im]
			}
		}

		_, _, _, resultx, _ := EvalTag3(wsegx, dict, features, preTags, rest)

		resultxx := GetWordTag(resultx)
		resultxx2 := CrfTrainSentence{}
		resultxx2.Sentence = append(resultxx2.Sentence, resultxx.Sentence[0])
		resultxx2.Label = append(resultxx2.Label, resultxx.Label[0])

		wordseg += StringWordTag(resultxx2)

		if ix == len(wtag.Sentence)-1 {
			break
		}

		if len(preTags) < 2 {
			preTags = append(preTags, resultxx2.Label[0])
		} else {
			preTags = preTags[1:]
			preTags = append(preTags, resultxx2.Label[0])
		}
	}

	return StringWordTag(GetWordTag(wordseg))
}

func EDUSegChunk(data string) []string {

	if data == "" {
		return make([]string, 0)
	}

	dict, err := GetNPChunkDictDB()
	if err != nil {
		return make([]string, 0)
	}

	features := LoadChunkFeature()

	eseg := GetWordTag(data)
	preTag := ""
	chunkResult := make([]string, 0)

	firstIndex, edusegSplit := EDUSplit(eseg, tokenSize)
	for k := range firstIndex {
		score, probx, chunk := EvalChunk(edusegSplit[k], dict, features, firstIndex[k], preTag)
		fmt.Printf("%s %d %f %f %s\n\n", edusegSplit[k], firstIndex[k], score, probx, chunk)
		preTag = chunk[len(chunk)-1]
		chunkResult = append(chunkResult, chunk...)
	}

	fmt.Printf("%s\n", chunkResult)

	return chunkResult
}

func EDUSegChunk2(data string) []string {

	if data == "" {
		return make([]string, 0)
	}

	dict, err := GetNPChunkDictDB()
	if err != nil {
		return make([]string, 0)
	}

	features := LoadChunkFeature()

	eseg := GetWordTag(data)
	preTag := ""
	chunkResult := make([]string, 0)

	firstIndex, edusegSplit := EDUSplit2(eseg, tokenChunkSize)
	//	fmt.Printf("Chunking - %s - %d\n", eseg.Sentence, len(eseg.Sentence))
	for k := range firstIndex {
		_, _, chunk := EvalChunk(edusegSplit[k], dict, features, firstIndex[k], preTag)
		// preTag = chunk[len(chunk)-1]
		// chunkResult = append(chunkResult, chunk...)
		preTag = chunk[0]
		chunkResult = append(chunkResult, chunk[0])
		//		fmt.Printf("%s ", preTag)
	}

	//	fmt.Printf("\n\n")

	return chunkResult
}

func EvalChunk(sentence CrfTrainSentence, dict Words, features []ChunkCrfFeature, firstIndex int, preTag string) (float64, float64, []string) {
	chunkTags := GenLabel(sentence.Label[firstIndex:], dict)
	for i := range chunkTags {
		if preTag != "" {
			chunkTagX := make([]string, 0)
			chunkTagX = append(chunkTagX, preTag)
			chunkTagX = append(chunkTagX, chunkTags[i]...)
			chunkTags[i] = chunkTagX
		}
	}
	score := make([]float64, len(chunkTags))
	prob := make([]float64, len(chunkTags))
	z := float64(0)

	for i := range chunkTags {
		score[i] = GetScoreChunkCRFx(chunkTags[i], sentence.Sentence, sentence.Label, firstIndex, features)
		z += math.Exp(score[i])
	}

	for i := range chunkTags {
		prob[i] = math.Exp(score[i]) / z
	}

	best := 0
	for i := range chunkTags {
		if prob[i] > prob[best] {
			best = i
		}
	}
	//	for i := range chunkTags {
	//		fmt.Printf("eval %s %f %f\n", chunkTags[i], score[i], prob[i])
	//	}

	lastIndex := firstIndex + tokenChunkSize
	if len(chunkTags[best]) != firstIndex+tokenChunkSize+1 {
		lastIndex = len(chunkTags[best])
	}
	return score[best], prob[best], chunkTags[best][firstIndex:lastIndex]
}

func EDUSplit(eseg CrfTrainSentence, tokenSize int) ([]int, []CrfTrainSentence) {
	firstIndex := make([]int, 0)
	chunkSplit := make([]CrfTrainSentence, 0)

	if len(eseg.Sentence) == 0 {
		return firstIndex, chunkSplit
	}

	fIndex := 0

	for {
		if fIndex == 0 {
			if len(eseg.Sentence) > tokenSize+1 {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex : tokenSize+1]
				cSplit.Label = eseg.Label[fIndex : tokenSize+1]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				fIndex = tokenSize
			} else if len(eseg.Sentence) > tokenSize {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex:tokenSize]
				cSplit.Label = eseg.Label[fIndex:tokenSize]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				fIndex = tokenSize
			} else {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex:len(eseg.Sentence)]
				cSplit.Label = eseg.Label[fIndex:len(eseg.Sentence)]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				break
			}
		} else {
			if len(eseg.Sentence) > fIndex+tokenSize+1 {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : fIndex+tokenSize+1]
				cSplit.Label = eseg.Label[fIndex-1 : fIndex+tokenSize+1]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				fIndex = fIndex + tokenSize
			} else if len(eseg.Sentence) > fIndex+tokenSize {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : fIndex+tokenSize]
				cSplit.Label = eseg.Label[fIndex-1 : fIndex+tokenSize]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				fIndex = fIndex + tokenSize
			} else {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : len(eseg.Sentence)]
				cSplit.Label = eseg.Label[fIndex-1 : len(eseg.Sentence)]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				break
			}
		}
	}
	return firstIndex, chunkSplit
}

func EDUSplit2(eseg CrfTrainSentence, tokenSize int) ([]int, []CrfTrainSentence) {
	firstIndex := make([]int, 0)
	chunkSplit := make([]CrfTrainSentence, 0)

	if len(eseg.Sentence) == 0 {
		return firstIndex, chunkSplit
	}

	fIndex := 0

	for {
		if fIndex == 0 {
			if len(eseg.Sentence) > tokenSize+1 {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex : tokenSize+1]
				cSplit.Label = eseg.Label[fIndex : tokenSize+1]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				fIndex++
			} else if len(eseg.Sentence) > tokenSize {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex:tokenSize]
				cSplit.Label = eseg.Label[fIndex:tokenSize]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				fIndex++
			} else {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex:len(eseg.Sentence)]
				cSplit.Label = eseg.Label[fIndex:len(eseg.Sentence)]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, fIndex)
				if fIndex == len(eseg.Sentence)-1 {
					break
				} else {
					fIndex++
				}
			}
		} else {
			if len(eseg.Sentence) > fIndex+tokenSize+1 {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : fIndex+tokenSize+1]
				cSplit.Label = eseg.Label[fIndex-1 : fIndex+tokenSize+1]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				fIndex++
			} else if len(eseg.Sentence) > fIndex+tokenSize {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : fIndex+tokenSize]
				cSplit.Label = eseg.Label[fIndex-1 : fIndex+tokenSize]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				fIndex++
			} else {
				cSplit := CrfTrainSentence{}
				cSplit.Sentence = eseg.Sentence[fIndex-1 : len(eseg.Sentence)]
				cSplit.Label = eseg.Label[fIndex-1 : len(eseg.Sentence)]
				chunkSplit = append(chunkSplit, cSplit)
				firstIndex = append(firstIndex, 1)
				if fIndex == len(eseg.Sentence)-1 {
					break
				} else {
					fIndex++
				}
			}
		}
	}
	return firstIndex, chunkSplit
}

func MakeChunk(line string, chunk []string) ChunkCrfSentence {
	chunkX := ChunkCrfSentence{}
	sentence := GetWordTag(line)
	chunkX.Sentence = sentence.Sentence
	chunkX.Label = sentence.Label
	chunkX.Chunk = chunk
	return chunkX
}

func MakeChunk1(lines []string, chunks [][]string) string {
	if len(lines) != len(chunks) {
		return ""
	}

	chunkOut := make([]ChunkCrfSentence, 0)

	for i := range lines {
		chunkOut = append(chunkOut, MakeChunk(lines[i], chunks[i]))
	}

	sentenceOut := ""

	for i := range chunkOut {
		if len(chunkOut[i].Label) != len(chunkOut[i].Chunk) {
			return ""
		}

		senOut := CrfTrainSentence{}
		senOut.Sentence = chunkOut[i].Sentence
		senOut.Label = make([]string, 0)
		for i2 := range chunkOut[i].Chunk {
			if IsChunk(chunkOut[i].Chunk[i2]) {
				senOut.Label = append(senOut.Label, chunkOut[i].Chunk[i2]+chunkOut[i].Label[i2])
			} else {
				senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
			}
		}
		sentenceOut += StringWordTagChunk(senOut) + "\n"
	}

	return sentenceOut
}

func MakeChunk2(lines []string, chunks [][]string) string {
	if len(lines) != len(chunks) {
		return ""
	}

	chunkOut := make([]ChunkCrfSentence, 0)

	for i := range lines {
		chunkOut = append(chunkOut, MakeChunk(lines[i], chunks[i]))
	}

	sentenceOutX := ""

	for i := range chunkOut {

		sentenceOut := CrfTrainSentence{}
		sentenceOut.Sentence = make([]string, 0)
		sentenceOut.Label = make([]string, 0)

		if len(chunkOut[i].Label) != len(chunkOut[i].Chunk) {
			return ""
		}

		senOut := CrfTrainSentence{}
		chunkLabel := ""
		for i2 := range chunkOut[i].Chunk {
			if IsChunk(chunkOut[i].Chunk[i2]) {
				if len(chunkOut[i].Chunk[i2]) == 1 {
					if chunkLabel != "" {
						chunkPhName := GetChunkPhName(chunkLabel)
						if chunkPhName == "" {
							fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
							return ""
						}
						sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
						sentenceOut.Label = append(sentenceOut.Label, chunkPhName)
					}
					chunkLabel = chunkOut[i].Chunk[i2]
					senOut = CrfTrainSentence{}
					senOut.Label = make([]string, 0)
					senOut.Sentence = make([]string, 0)
					senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
					senOut.Sentence = append(senOut.Sentence, chunkOut[i].Sentence[i2])
				} else {
					if chunkLabel != string(chunkOut[i].Chunk[i2][0]) {
						fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
						return ""
					}
					senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
					senOut.Sentence = append(senOut.Sentence, chunkOut[i].Sentence[i2])
				}

			} else {
				if chunkLabel != "" {

					chunkPhName := GetChunkPhName(chunkLabel)
					if chunkPhName == "" {
						fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
						return ""
					}
					sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
					sentenceOut.Label = append(sentenceOut.Label, chunkPhName)
				}
				chunkLabel = ""
				sentenceOut.Label = append(sentenceOut.Label, chunkOut[i].Label[i2])
				sentenceOut.Sentence = append(sentenceOut.Sentence, chunkOut[i].Sentence[i2])
			}
		}

		if len(senOut.Sentence) > 0 && chunkLabel != "" {
			sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
			sentenceOut.Label = append(sentenceOut.Label, GetChunkPhName(chunkLabel))
		}

		sentenceOutX += StringWordTag(sentenceOut) + "\n"
	}

	return sentenceOutX
}

func MakeChunk2x(lines []string, chunks [][]string) string {
	if len(lines) != len(chunks) {
		return ""
	}

	chunkOut := make([]ChunkCrfSentence, 0)

	for i := range lines {
		chunkOut = append(chunkOut, MakeChunk(lines[i], chunks[i]))
	}

	sentenceOutX := ""

	for i := range chunkOut {

		sentenceOut := CrfTrainSentence{}
		sentenceOut.Sentence = make([]string, 0)
		sentenceOut.Label = make([]string, 0)

		if len(chunkOut[i].Label) != len(chunkOut[i].Chunk) {
			return ""
		}

		senOut := CrfTrainSentence{}
		chunkLabel := ""
		for i2 := range chunkOut[i].Chunk {
			if IsChunk(chunkOut[i].Chunk[i2]) {
				if len(chunkOut[i].Chunk[i2]) == 1 {
					if chunkLabel != "" {
						chunkPhName := GetChunkPhName(chunkLabel)
						if chunkPhName == "" {
							fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
							return ""
						}
						sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
						sentenceOut.Label = append(sentenceOut.Label, chunkPhName)
					}
					chunkLabel = chunkOut[i].Chunk[i2]
					senOut = CrfTrainSentence{}
					senOut.Label = make([]string, 0)
					senOut.Sentence = make([]string, 0)
					senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
					senOut.Sentence = append(senOut.Sentence, chunkOut[i].Sentence[i2])
				} else {
					if chunkLabel != string(chunkOut[i].Chunk[i2][0]) {
						//fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
						//return ""
						chunkPhName := GetChunkPhName(chunkLabel)
						if chunkPhName == "" {
							sentenceOut.Label = append(sentenceOut.Label, chunkOut[i].Label[i2])
							sentenceOut.Sentence = append(sentenceOut.Sentence, chunkOut[i].Sentence[i2])

							//	fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
							//	return ""
						} else {
							sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
							sentenceOut.Label = append(sentenceOut.Label, chunkPhName)
						}
						chunkLabel = string(chunkOut[i].Chunk[i2][0])
						senOut = CrfTrainSentence{}
						senOut.Label = make([]string, 0)
						senOut.Sentence = make([]string, 0)
						senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
						senOut.Sentence = append(senOut.Sentence, chunkOut[i].Sentence[i2])

					} else {
						senOut.Label = append(senOut.Label, chunkOut[i].Label[i2])
						senOut.Sentence = append(senOut.Sentence, chunkOut[i].Sentence[i2])
					}
				}

			} else {
				if chunkLabel != "" {

					chunkPhName := GetChunkPhName(chunkLabel)
					if chunkPhName == "" {
						fmt.Printf("%s - %s \nError at line %d col %d\n", chunkOut[i], chunkOut[i].Chunk[i2], i+1, i2+1)
						return ""
					}
					sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
					sentenceOut.Label = append(sentenceOut.Label, chunkPhName)
				}
				chunkLabel = ""
				sentenceOut.Label = append(sentenceOut.Label, chunkOut[i].Label[i2])
				sentenceOut.Sentence = append(sentenceOut.Sentence, chunkOut[i].Sentence[i2])
			}
		}

		if len(senOut.Sentence) > 0 && chunkLabel != "" {
			sentenceOut.Sentence = append(sentenceOut.Sentence, StringWordTag(senOut))
			sentenceOut.Label = append(sentenceOut.Label, GetChunkPhName(chunkLabel))
		}

		sentenceOutX += StringWordTag(sentenceOut) + "\n"
	}

	return sentenceOutX
}

func GetChunkPhName(c string) string {
	switch c {
	case "*":
		return "HNpat"
	case "+":
		return "ADJpat"
	case "@":
		return "AMTpat"
	case "#":
		return "VRBpat"
	case "$":
		return "ADVpat"
	case "&":
		return "VNNpat"
	case "%":
		return "VRIpat"
	case "!":
		return "PRPpat"
	case "=":
		return "TIMEpat"
	case "?":
		return "DETpat"
	case "-":
		return "CLSpat"
	default:
		return ""
	}
}

func IsChunk(dat string) bool {
	switch dat {
	case "*", "**", "+", "++", "@", "@@", "#", "##", "$", "$$", "%", "%%", "!", "!!", "&", "&&", "=", "==", "?", "??", "-", "--":
		return true
	default:
		return false
	}
}

func StringWordTag(data CrfTrainSentence) string {
	return WordTagString(data)
}

func StringWordTagChunk(data CrfTrainSentence) string {
	result := ""
	for k := range data.Sentence {
		if IsTag(data.Sentence[k]) {
			if IsChunk(string(data.Label[k][0])) {
				result += "<" + data.Label[k] + ">"
			} else {
				result += data.Sentence[k]
			}
			continue
		}
		result += "[" + data.Sentence[k] + "]<" + data.Label[k] + ">"
	}
	return result
}

func CutSpaceLine(datax string) []string {
	data := RemoveSpace(datax)
	idx := 0
	addPrev := false
	result := make([]string, 0)
	for {
		spx := FindSpace(data, idx)
		if spx == -1 {
			if addPrev {
				result[len(result)-1] = result[len(result)-1] + data[idx:len(data)]
				addPrev = false
			} else {
				result = append(result, data[idx:len(data)])
			}
			break
		}

		if IsForeign1(string(data[spx-1])) {
			if IsForeign1(string(data[spx+7])) {
				result = append(result, data[idx:spx+7])
				idx = spx + 7
				addPrev = true
				continue
			}
		}

		if addPrev {
			result[len(result)-1] = result[len(result)-1] + data[idx:spx]
			addPrev = false
		} else {
			result = append(result, data[idx:spx])
		}
		idx = spx + 7

	}
	return result
}
