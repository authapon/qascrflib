module gitlab.com/authapon/qascrflib

go 1.16

require (
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	gitlab.com/authapon/moosqlite v1.0.0
)
