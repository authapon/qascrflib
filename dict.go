package qascrflib

import (
	"errors"
	moosql "gitlab.com/authapon/moosqlite"
	"strings"
)

func AddWord(word, pos string, dict Words) {
	w, ok := dict[word]
	if !ok {
		dict[word] = []string{pos}
	} else {
		for _, v := range w {
			if v == pos {
				return
			}
		}
		w = append(w, pos)
		dict[word] = w
	}
}

func GetDictDB() (Words, error) {
	dict := make(Words)
	db, err := moosql.GetSQL()
	if err != nil {
		return dict, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `word`, `pos` from `dict`;")
	rows, err := st.Query()
	if err != nil {
		return dict, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		word := ""
		pos := ""
		rows.Scan(&word, &pos)
		posx := strings.Split(pos, " ")
		for _, v := range posx {
			AddWord(word, v, dict)
		}
	}
	return dict, nil
}

func GetNPChunkDictDB() (Words, error) {
	dict := make(Words)
	db, err := moosql.GetSQL()
	if err != nil {
		return dict, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `pos`, `tag` from `npchunkdict`;")
	rows, err := st.Query()
	if err != nil {
		return dict, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		word := ""
		pos := ""
		rows.Scan(&word, &pos)
		posx := strings.Split(pos, " ")
		for _, v := range posx {
			AddWord(word, v, dict)
		}
	}
	return dict, nil
}
